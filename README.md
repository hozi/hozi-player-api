# Hozi Player API

## Description
This project containes the API for hozi player, in order for other websites to integrate with it.
It allow selecting video, controlling it's playack (play/pause), seeking, etc.

## Implementations Details
The API based on the window.postMessage API, using post-robot library. 
The player is listening to certain commands via the Post-Robot library and reacts. 
This library is bundeld using webpack.

## Installation
``` npm install hozi-player-api```

## Getting started
Add component with Iframe template.
You can read more about Iframe src options here - https://gitlab.com/hozi/hozi-player-ui 

``` 
    import { HoziPlayer, HoziPlayerApi } from 'hozi-player-api';

    @Component({
        selector: 'getting-started,
        template: `<iframe allowfullscreen="true" frameborder="0" scrolling="no" width="500px" height="368px" 
          src="https://hozi-player-ui.web.app/embd"></iframe>`
    })

    class GettingStartedComponent implements OnInit {

      private player: HoziPlayer;

      constructor() { }

      ngOnInit() {
        const iframe: HTMLIFrameElement = document.getElementsByTagName("iframe").item(0) as HTMLIFrameElement;

        this.player = new HoziPlayerApi(
          iframe,
          {
            timeout: 20000,
            events: {
              onReady: () => console.log("iframe loaded!", "close"),
              onVideoLoaded: (videoId: string) => console.log("Video loaded! - " + videoId, "close")
            }
          }
        )
      }

      play() {
        this.player.playVideo();
      }

      stop() {
        this.player.stopVideo();
      }

      async reverse() {  
        await this.player.reverse();
      }

      loadVideo(selectedId: string) {
        if (selectedId) {
          this.player.loadVideoById(selectedId);
        }
      }

      async seek(seconds: number) {  
        await this.player.seekVideo(seconds);
      }
  }
```

## Api

Controlling Player

* playVideo()
* stopVideo()
* reverse()
* seekVideo(seconds: number)

Loading video
* loadVideoById(videoId: string)
