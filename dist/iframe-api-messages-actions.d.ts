export declare const IframeActions: {
    readonly PLAY: "play";
    readonly PAUSE: "pause";
    readonly STOP: "stop";
    readonly REVERSE: "reverse";
    readonly LOAD_VIDEO: "loadVideo";
    readonly GET_DURATION: "getDuration";
    readonly TAKE_SCREENSHOT: "takeScreenshot";
    readonly SEEK: "seek";
    readonly GET_VIDEO_ID: "getVideoId";
    readonly GET_CURRENT_TIME: "getCurrentTime";
    readonly VIDEO_LOADED: "videoLoaded";
    readonly ON_READY: "onReady";
    readonly LOAD_VIDEO_BY_IP_AND_ABS_TIME: "loadVideoByIpAndAbsTime";
    readonly SEEK_ABS_TIME: "seekAbsTime";
    readonly VIDEO_TIME: "videoTime";
};
