export declare const TIMEOUT = 20000;
declare type HoziPlayerApiOptions = {
    timeout?: number;
    events?: {
        onReady?: () => void;
        onVideoLoaded?: (videoId: string) => void;
        onVideoTimeUpdate?: (event: {
            sourceId: string;
            seekbarStartTime: number;
            seekbarEndTime: number;
            currentTime: number;
        }) => void;
    };
};
export declare class HoziPlayerApi {
    iframe: HTMLIFrameElement;
    private onVideoLoadedListener;
    private onVideoTimeUpdateListener;
    private timeout;
    constructor(iframe: HTMLIFrameElement, options?: HoziPlayerApiOptions);
    private onReady;
    playVideo(): Promise<any>;
    stopVideo(): Promise<any>;
    reverse(): Promise<any>;
    loadVideoById(videoId: string): Promise<any>;
    getVideoDuration(): Promise<any>;
    loadVideoByIpAndAbsTime(videoId: string, startFrom: number): Promise<any>;
    seekAbsTime(time: number): Promise<any>;
    seekVideo(second: number): Promise<any>;
    getCurrentVideoId(): Promise<number>;
    onVideoLoaded(callback: Function): void;
    onVideoTimeUpdate(callback: Function): void;
}
export {};
