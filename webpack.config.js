const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const path = require("path");

const config = {
    context: __dirname + '\\src',
    resolve: {
        extensions: [".ts", ".js"]
    },
    entry: {
        "hozi-player-api": './index.ts',
        "hozi-player-api.min": './index.ts'
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].bundle.js',
        library: "hozi-player-api",
        libraryTarget: "umd"
    },
    performance: {
        hints: "warning"
    },
    devtool: "source-map",
    module: {
        rules: [{
            test: /\.(ts|js)$/,
            use: ['ts-loader'],
            exclude: [/node_modules/, /assets/, path.resolve(__dirname, 'assets')]
        }]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new UglifyJsPlugin({
            include: /^hozi-player-api.min/
        })
    ]
}

module.exports = config;