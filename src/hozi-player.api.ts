import { HoziPlayerApi } from ".";

export class HoziPlayer extends HoziPlayerApi {

    constructor(public iframe: HTMLIFrameElement) {
        super(iframe)
    }
}