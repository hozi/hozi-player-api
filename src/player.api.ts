import * as postRobot from 'post-robot';
import { IframeActions } from './iframe-api-messages-actions';

export const TIMEOUT = 20000;

type HoziPlayerApiOptions = {
    timeout?: number,
    events?: {
        onReady?: () => void,
        onVideoLoaded?: (videoId: string) => void
        onVideoTimeUpdate?: (event: {
            sourceId: string,
            seekbarStartTime: number,
            seekbarEndTime: number,
            currentTime: number
        }) => void
    }
}

export class HoziPlayerApi {
    private onVideoLoadedListener: any;
    private onVideoTimeUpdateListener: any;
    private timeout = TIMEOUT;

    constructor(public iframe: HTMLIFrameElement, options?: HoziPlayerApiOptions) {
        if (options) {
            const { timeout, events } = options;
            if (timeout) {
                this.timeout = timeout;
            }
            if (events) {
                const { onVideoLoaded, onReady, onVideoTimeUpdate } = events;
                if (onVideoLoaded) {
                    this.onVideoLoaded(onVideoLoaded);
                }
                if (onReady) {
                    this.onReady(onReady);
                }
                if (onVideoTimeUpdate)
                    this.onVideoTimeUpdate(onVideoTimeUpdate)
            }
        }
    }

    // private function
    private onReady(callback: () => void) {
        postRobot.on(IframeActions.ON_READY, () => callback());
    }

    async playVideo() {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.PLAY, null, { timeout: this.timeout });
        return data;
    }

    // async pauseVideo() {
    //     const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.PAUSE, null, { timeout: this.timeout });
    //     return data;
    // }

    async stopVideo() {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.STOP, null, { timeout: this.timeout });
        return data;
    }

    async reverse() {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.REVERSE, null, { timeout: this.timeout });
        return data;
    }

    async loadVideoById(videoId: string) {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.LOAD_VIDEO, videoId, { timeout: this.timeout });
        return data;
    }

    async getVideoDuration() {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.GET_DURATION, null, { timeout: this.timeout });
        return data;
    }
    async loadVideoByIpAndAbsTime(ip: string, startFrom: number) {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.LOAD_VIDEO_BY_IP_AND_ABS_TIME, { videoId: ip, startFrom }, { timeout: this.timeout });
        
        return data;
    }
    async seekAbsTime(time: number) {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.SEEK_ABS_TIME, null, { timeout: this.timeout });
        return data;
    }

    // async takeScreenshot(autoDownload: boolean) {
    //     const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.TAKE_SCREENSHOT, autoDownload, { timeout: this.timeout });
    //     return data;
    // }

    async seekVideo(second: number) {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.SEEK, second, { timeout: this.timeout });
        return data;
    }

    async getCurrentVideoId() {
        const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.GET_VIDEO_ID, null, { timeout: this.timeout });
        return data as number;
    }

    // async getVideoCurrentTime() {
    //     const { data } = await postRobot.send(this.iframe.contentWindow, IframeActions.GET_CURRENT_TIME, null, { timeout: this.timeout });
    //     return data as number;
    // }

    onVideoLoaded(callback: Function) {
        if (this.onVideoLoadedListener) {
            this.onVideoLoadedListener.cancel();
        }
        this.onVideoLoadedListener = postRobot.on(IframeActions.VIDEO_LOADED, (event: { data: any }) => callback(event.data));
    }
    onVideoTimeUpdate(callback: Function) {
        if (this.onVideoTimeUpdateListener) {
            this.onVideoTimeUpdateListener.cancel();
        }
        this.onVideoTimeUpdateListener = postRobot.on(IframeActions.VIDEO_TIME, (event: { data: any }) => callback(event.data));
    }
}