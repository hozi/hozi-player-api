export const IframeActions = {
    PLAY: "play",
    PAUSE: "pause",
    STOP: "stop",
    REVERSE: "reverse",
    LOAD_VIDEO: "loadVideo",
    GET_DURATION: "getDuration",
    TAKE_SCREENSHOT: "takeScreenshot",
    SEEK: "seek",
    GET_VIDEO_ID: "getVideoId",
    GET_CURRENT_TIME: "getCurrentTime",
    VIDEO_LOADED: "videoLoaded",
    ON_READY: "onReady",
    LOAD_VIDEO_BY_IP_AND_ABS_TIME: "loadVideoByIpAndAbsTime",
    SEEK_ABS_TIME: "seekAbsTime",
    VIDEO_TIME: "videoTime"
} as const;